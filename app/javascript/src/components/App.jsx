import React from 'react'
import bootstrap from 'bootstrap'
import { Route, Switch } from 'react-router-dom'
import Home from '../components/Home'
import Posts from '../components/posts/Posts'
import Head from '../components/partials/head'

const App = () => {
  return (
    <div className='ml-4'>
      <Head />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/posts" component={Posts} />
      </Switch>
    </div>
  )
}

export default App
