import React from 'react'

class Posts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      posts: []
    }
  }

  componentDidMount() {
    this.fetchPosts()
  }

  fetchPosts() {
    fetch(`/api/articles`, {
        credentials: 'same-origin'
      }).then(response => response.json()).then(response => {
        this.setState({
          posts: response,
          loaded: true
        })
      })
    }

  render() {
    return (
      <div>
        <h1>Posts</h1>
        {this.state.posts.map( (post) => {
          return (
            <div key={post.id}>
              <h3>
                {post.id}. {post.title}
              </h3>
              <p>{post.body}</p>
            </div>
          )
        })}
      </div>
    )
  }
}

export default Posts
