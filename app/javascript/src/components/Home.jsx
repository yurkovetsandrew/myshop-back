import React from 'react'
import Head from '../components/partials/head'

class Home extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <div>
        <div className='image-first'>
          <h1 className='image-description'>
            Disconnect. Relax.
          </h1>
        </div>
        <p className='text-description'>Previously, people did not know anything about space, about the stars and believed that the sky is a cap that
covers the Earth, and the stars are attached to it. Ancient people thought that the Earth was motionless, and
The sun and moon revolve around it.</p>
        <hr/>
        <div className='featured-collection'></div>
          <div className='image-second'>
            <h1 className='image-description'>100% Europe</h1>
            <p className='image-description-paragraph'>While most outdoor clothes are made in Asia and shipped for weeks in fossil fueled cargo ships, Space store’s products are all made in Europe from European fabrics, most of which are recycled and sustainable.</p>
          </div>
      </div>
    )
  }
}

export default Home
