import React from 'react'
import Home from '../Home'


class Head extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let scrollpos = window.scrollY
    const headEl = document.querySelectorAll('.head-elem')
    const scrollChange = 500
    const addClassOnScroll = () => {
      document.querySelector('.head').classList.add('head-active')
      headEl.forEach((el) => {
        el.classList.add('white-text')
      })
      document.querySelector('.head-elem-first h3').classList.add('white-text')
    }
    const removeClassOnScroll = () => {
      document.querySelector('.head').classList.remove('head-active')
      headEl.forEach((el) => {
        el.classList.remove('white-text')
      })
      document.querySelector('.head-elem-first h3').classList.remove('white-text')
    }
    window.addEventListener('scroll', () => {
      scrollpos = window.scrollY
      if (scrollpos >= scrollChange) { addClassOnScroll() }
      if (scrollpos <= scrollChange) { removeClassOnScroll() }
    })
  }

  render() {

    return (
      <div className='head'>
        <a className='head-elem-first'>
          <h3>SPACE STORE</h3>
        </a>
        <a className='head-elem' href="#">WOMEN'S</a>
        <a className='head-elem'>MEN'S</a>
        <a className='head-elem'>BLOG</a>
        <a className='head-elem'>JOIN OUT COMMUNITY</a>
        <div className='head-rigth'>
          <a className='head-elem'>Cart</a>
          <a className='head-elem'>Account</a>
        </div>
      </div>
    )
  }
}

export default Head
